temporary-email
===============

Temporary E-mail Installation and Configuration Guide
1      Introduction
2      Features
2.1       Front End
2.1.1       Automatic E-mail Check
2.2       Outline
2.3       Admin Area
2.3.1       Homepage
2.3.2       Admin Account
2.3.3       Settings
3      Installation
3.1       Step-by-Step Guide
3.2       Upload
3.3       File and Folder Permissions
3.4       Database Access Data
4      Update to the Latest Version
4.1       Step-by-Step Guide
5      Layout Customization
5.1       Create New Template Set
6      Language Files
7      Backup
7.1       Export
7.2       Import
7.3       Download
7.4       Delete
8      Feature Modules
8.1       Installation
8.2       Available Modules
8.2.1       Akismet
8.2.2       CAPTCHA
8.2.3       IP Block
8.2.4       Referrer Log
8.2.5       Social Links
8.2.6       Text Link Ads
8.2.7       AuctionAds
9      Help and Support
 

1         Introduction
 
Thanks for using GentleSource Temporary E-mail. The script creates and manages disposable e-mail addresses that expire after a certain time. You can read and reply to e-mails that are sent to the temporary e-mail address within the given time frame.
 
The script requires either PHP/4 or PHP/5 on Unix/Linux/Windows and a MySQL database. You also need a software (WinZIP for example) to unpack the script files after downloading them from our server. You should have a solid understanding of HTML and some experience with script installation.
2         Features
2.1        Front End

With a click on the button “Get temporary e-mail” the user generates a new e-mail address. The button “Set temporary e-mail” lets the user create a self chosen e-mail address. That address will be stored in the database along with its creation time. The e-mail address is assigned to the user by a session cookie.
 
The user may then use the e-mail for any purpose where an e-mail address is required. Most commonly that will be some kind of registration.
 
2.1.1        Automatic E-mail Check

The script checks continuously for arriving e-mails. Once an e-mail that has been sent to a generated e-mail address arrives, the script displays it.
 
Learn more about how to configure the automatic e-mail check in chapter ‎2.3.3.2.8.
2.2        Outline

·        User can create random e-mail addresses

·        User can set a certain e-mail address

·        User can see incoming e-mails

·        User can reply to e-mails

·        User can delete e-mails

·        User can subscribe to RSS feed of an e-mail

·        Completely template driven

·        Installation routine

2.3        Admin Area

2.3.1        Homepage

The admin area homepage shows a few statistic figures. There is a distinction between e-mail and e-mail address. E-mail is the message that one can send or receive. E-mail address is the address one can e-mails send to.
 
Created Addresses
Number of times visitors have clicked the button "Get temporary e-mail" and created an e-mail address.
 
E-mails Received
Number of e-mail messages that have been downloaded from the mail account.
 
E-mails Valid
Valid e-mails are e-mail messages that have arrived within the time frame of an active e-mail address. While downloading the e-mail messages from the mail account, the script checks if the recipient address (e.g. ekktuic@tempmaildemo.com) is in the database. If it is, the e-mail message is valid and will be displayed to the user.
 
E-mails Expired
All e-mail messages that are not valid because the time has run out and the e-mail address has been deleted. E-mails expired does not mean the expired e-mail addresses.
 
I.e. "E-mails expired" actually means "E-mail messages that have been received after the e-mail address has expired".
 
E-mails Sent
Number of e-mail messages that have been sent using the reply feature.
 
 
 
 

2.3.2        Admin Account

This section allows you to change login name, e-mail, and login password.
2.3.3        Settings

2.3.3.1       General Settings
2.3.3.1.1      Admin area and front end language
 
You can set the default language for both areas of the script. That setting will be overwritten, if the user’s browser language settings are different or the user selects a language manually. The changed language setting will be stored in a cookie.
 
2.3.3.1.2      Display language selection
 
Set this option to “No” if you want to hide the language menu.
 
2.3.3.1.3      Use UTF-8 language files
 
The folder /language/ contains the subfolder /utf8/ which contains UTF-8 encoded text. If you set this option to “Yes”, the script will use the language files from the subfolder.
 
If you want to use a non-latin language with the script, UTF-8 is your choice.
 
2.3.3.1.4      Tool Shut Down
 
You can shut the whole tool down, e.g. for maintenance. During the shut down the front end functions will not be available for the users, but the administrator can login to the admin area.
 
2.3.3.1.5      Display turn off message
 
If set to “Yes” you can let the users know that the tool has been shut down. Otherwise they see just a blank screen.
 
2.3.3.2       Mail Settings
2.3.3.2.1      E-mail Lifetime
 
By default the e-mail lifetime is set to 15 minutes. You can of course change the lifetime and even can set it to weeks, months and years.
 
2.3.3.2.2      Set E-mail Address
 
Instead of a completely automatically generated e-mail address, your visitors can choose the first part of the e-mail address as they like.
 
2.3.3.2.3      Activate E-mail RSS/Atom Feed
 
Visitors can subscribe to an RSS/Atom feed of their e-mail address. This way they will receive all new e-mails in their feed reader.
 
2.3.3.2.4      E-mail Account
 
In order for the script to receive e-mails, an e-mail account (mailbox) must be available. It is important that the e-mail account is configured as catch-all. Each web host handles the catch-all feature differently. Please contact your web host about that, if you do not know whether your web space package supports catch-all or how it can be turned on.
 
If your web space manager is called "cPanel X" you need to create a mail account first and then go to Mail >> Default Address. If there is nothing set yet, click on the link "Set Default Address" and enter the name of the e-mail account you want to use with the script.
 
2.3.3.2.5      Use SSL to connect to mailbox
 
If you want to connect to your mailbox through SSL and you mailbox supports SSL, set this option to “Yes”.
 
2.3.3.2.6      Mail Certificate Issues
 
Some mail accounts want to validate a certificate and end the connection with an error message. In that case you should set the option to “Yes”.
 
2.3.3.2.7      Mail Download Key
 
The key is kind of a password to make sure that no third party knows the exact URL that is called to check for incoming e-mails. If the key in the URL does not match the key in the settings, the script will not process the mail download.
 
2.3.3.2.8      Automatic e-mail check
 
The script uses the e-mail account data (chapter ‎2.3.3.2.2) to download incoming e-mails and display those e-mails to the users. The e-mail check requires a certain URL of the script to be called continuously.
 
You can find this URL in the admin area -> Configuration -> General Settings -> Miscellaneous (bottom of the page) -> Mail download URL.

If your web space package contains crontab, you can create a cron job that calls the mail download URL.
 
If that is not the case you can use the cron job that the script provides. You can find it in the admin area -> Configuration -> Cron Job. Put the URL where it says URL and set the time between each URL call. We have found that 60 seconds is a good figure.
3         Installation
3.1        Step-by-Step Guide

The script has been configured in a way that makes it ready to use immediately. Please follow these steps:
 
1.      Connect to your web server using an FTP program.

2.      Create a new folder on your server named e.g. temporary_email. This way the script will be installed in a sub-directory. If you want to install the script directly in the root folder, just upload all files and folders to the root folder. It is not a requirement of the script to be installed in a sub-directory.

3.      Upload the whole script including all files and folders to your server into the new folder. Make sure the original file structure appears on the server. Read more details in chapter ‎3.2 Upload.

4.      Make sure that the script folder and the sub-folder cache are writable by the script (chmod 777). Read more details in the chapter ‎3.3 File and Folder Permissions. If those folders are not writable the installation will fail.

5.      Call the admin area of the script in your browser:
http://www.example.com/temporary_email/admin/

6.      You are probably seeing now the installation screen of the script. Please make sure you have the database access data. Read more details in chapter ‎3.4 Database Access Data.

7.      Choose a prefix for the database tables. Default value is t12l_ and there is actually no need to change that unless you want to have more than one installation of the script side by side.

8.      Enter the information for the admin account you want to use. You can choose the username and password you like.

9.      Click the button “Install Now”. In case the installation was successful, you will be provided with a link to the login screen where you can use the account data you just entered.

10.  You can change the permissions of the script folder back to the value before, but the permissions of the folder cache must remain 777.

3.2        Upload

All files, except for image files, have to be uploaded in text mode (ASCII mode). Images files (gif, jpg, png) have to be uploaded in binary mode. If your FTP program allows you to set the transfer mode to automatic, you should do so.
3.3        File and Folder Permissions

Most, if not all, FTP programs allow you to change the file permissions. That can either be done with a command line and the command chmod or with some clicks on menus and checkboxes.
 
We have published a guide with examples of common FTP programs on our website.
 
http://gentleurl.net/chmod/
3.4        Database Access Data

You need following four items:
·        Database hostname

·        Database name

·        Database user name

·        Database password

 
You either get those data from your web space control panel or ask your web space provider about them.
4         Update to the Latest Version
4.1        Step-by-Step Guide

Please make a backup of all your files, especially those in the folders /configuration/ and /template/. During the update process all existing files will be overwritten and all changes will be lost after that.
 
Please follow these steps:
 
1.      Login to the admin area of the script and go to the backup section. Export a backup.

2.      Connect to your web server using an FTP program.

3.      Download the whole script folder to your local hard disk. This way you have a backup of all files and database tables in case the update fails and files or data are deleted.

4.      Upload all files and folders of the new script version to your server into the folder that contains the existing script. Please be aware that all files are now overwritten by the new files. Make sure the original file structure appears on the server. Read more details in chapter ‎3.2 Upload.

5.      Make sure the script folder and the sub-folder /cache/ are writable by the script (chmod 777). Read more details in the chapter ‎3.3 File and Folder Permissions. If those folders are not writable the installation will fail.

6.      Call the admin area of the script in your browser, e.g.:

http://www.example.com/temporary_email/admin/


7.      You are probably now seeing the update screen of the script. Please make sure you have the database access data at hand. Read more details in chapter ‎4.4 Database Access Data.

8.      Enter the login data of the admin account.

9.      Click the button Update. In case the update was successful, you will be provided with a link to the login screen where you can login as usual.

 
5         Layout Customization
 
The script uses a template system that makes it easy to customize the layout. In the folder /template/default/ reside all the HTML files that contain the layout of the front end.
 
layout.tpl.html
Contains the layout of the front page.
 
 
email.tpl.html
Contains the details.
 
 
style.css
Contains the styles.
 
 
emailmessage.tpl.html
If the user has JavaScript enabled, this template will be used to display the e-mail message.
 
 
emailmessagelist.tpl.html
If the user has JavaScript enabled, this template will be used to display the e-mail message list.
 
 
emailaddress.tpl.html
If the user has JavaScript enabled, this template will be used to display the e-mail address.
 
 
language.tpl.html
Contains the language selection.
 
5.1        Create New Template Set

In order to create a new template set, create a new folder (i.e. /my_design/) inside the directory /template/. Copy at least the files layout.tpl.html and style.css from the folder default into your new folder. If you want to customize the complete page including the detail template, you have to copy the file email.tpl.html too.
 
Open the file index.php and add following line of code on top of the file (but below <?php).
 
define('T12L_ALTERNATIVE_TEMPLATE', 'my_design');
 
Important: Replace my_design with the name of your layout folder.
6         Language Files
 
All text can be found at one central place – the language files. For each language one file resides in the folder /language/. You can edit the files using a text editor.
 
In case you want to create a new language file, follow these steps:
 
1.      Copy one of the existing language files and change the language abbreviation.

2.      Open the new language file with a file editor and make your changes.

3.      Open the file /configuration/default.inc.php and look for the option 

$t12l['available_languages']

Add your language to the array, i.e.:

$t12l['available_languages']     = array(
                                   'en' => 'English',
                                   'de' => 'German', 
                                   'es' => 'Spanish',
                                   );


4.      The new language should now appear in the admin area -> general settings.

 
Please note: The steps are the same for the language files inside the folder utf8, except that you need to save the files formatted as UTF-8 without BOM. If your editor only offers the option UTF-8, you need to open the language file as ASCII and remove the characters that are at the beginning of the file in front of <?php.
 
Once you have finished the translation of a file you could support us by sending in that file so that we can add it to the official version of the script. Thanks in advance.
7         Backup
 
The backup section of the admin area allows you to export, import, download or delete a backup file. A backup file contains all data from the database including articles and settings.
 
7.1        Export

When you click “Export Backup” the script will write all data into a file in the folder /cache/backup/. The file name consists of the date (ISO standard) and time of the export. The content is formatted in MySQL statements which make it possible for you to import it using any other MySQL administration tool such as PHPMyAdmin.
7.2        Import

If you want to import a backup, simply select the one you intend to import from the list and click “Import Backup”.
 
Attention: Importing a backup will delete (as in completely destroy) all existing data. It is advised to export a backup first.
7.3        Download

This option allows you to save a local copy of the export file. This way you can upload the backup to your server in case of a full data loss including all files on your server.
7.4        Delete

This option will delete a backup file.
 
 
Please note: The admin account information (login name, e-mail address and password) are neither exported nor imported in order to make sure those information do not change unexpectedly when you work with backups.
8         Feature Modules
 
Modules are small tools that are integrated into the script and add various kind of functionality.
 
These tools reside in the folder /modules/. Each module has its own language file and template file. You can find those files in the folders
 
/module/gentlesource_module_*/language/
 
and
 
/module/gentlesource_module_*/template/.
 
Not every module is available for every script. For example, it makes no sense to include Captcha or flood protection to a script that has no user contribution capability such as commenting or sending e-mails.
 
Please note: Some modules (e.g. Captcha, IP-Block) offer the option to moderate comments. Some scripts (e.g. Temporary E-Mail) do not support the moderation option, but only the reject option.
8.1        Installation

 
By default most of the modules are not installed. In order to install a module, login to the admin area and open the configuration section. Click “Module Installation” to open a list with available modules.
 
Install and uninstall modules as you wish. You are also able to change the position in the list.
8.2        Available Modules

8.2.1        Akismet

Akismet.com analyzes incoming comments and checks if that comment has been listed as Spam. You need to get an API Key from http://www.akismet.com/ by registering an account at http://www.wordpress.com/. If you leave this API key empty, Akismet will not be used.
8.2.2        CAPTCHA

The Captcha feature displays an image with a string of characters. In order to prove that the visitor is not an automatic spam program, he has to enter the characters shown in the image into a form field. In case both strings match the comment will be accepted. 
8.2.3        IP Block

Moderate or block comments based on the IP address of the visitor.
8.2.4        Referrer Log

The module writes date, time referrer and request URI of a visit to a text file.
8.2.5        Social Links

Enabling this feature will display a list with links to social services such as del.icio.us or digg below the page content. You can edit the list in the template link.tpl.html in the module folder.
8.2.6        Text Link Ads

Make money with your website and publish simple text link ads or buy text link ads to improve your traffic and search engine rankings. You can find more information at Text Link Ads: http://www.text-link-ads.com/?ref=37444.
 
In order to use this feature you have to register with Text Link Ads.
8.2.7        AuctionAds

Make money with your website and publish ads from AuctionAds. More information at  http://www.auctionads.com/refer_9ed4505eaeb7d99a79d6.
 
Publisher ID
 
You can find your publisher ID in your AuctionAds account. It’s listed on "Account Home".
 
Campaign ID
 
The campaign ID is a string of letters and numbers. It is not the text that you entered when you created the campaign. It is automatically generated by AuctionAds when you create a new campaign.
 
The campaign ID is not easy to obtain. In fact it is not shown in your Auction Ads account in plain text. There are two ways to find the campaign ID. Go to the campaign manager in your AuctionAds account. View the source text and look for the checkboxes. The value attribute contains the campaign ID. The other way would be – in Firefox – to view the page info. There is a tab named "Forms" that contains all form fields and values listed.
 
Keywords
 
AuctionAds has published some helpful information about keywords. You should really check them out, but in short: Separate keywords by comma, separate keyword groups by semicolon, put a minus sign in front of keywords you want to exclude.
 
Styles
 
If you are familiar with CSS, you can enter CSS code to format or position the ad block.
 
9         Help and Support
 
If you run into problems while installing or configuring News Script, please use our support forum on http://www.gentlesource.com/.
 
